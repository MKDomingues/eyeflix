import UIKit

final class EFCollectionViewHeader: UICollectionReusableView {
    
    private let label = UILabel()
    
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        self.setLabel()
    }
    
    required init?(coder: NSCoder) {
        fatalError("🧨 init(coder:) has not been implemented")
    }
    
    private func setLabel() {
        
        label.textColor = .EFTextFadedRed
        label.font = UIFont(name: "HelveticaNeue-Medium", size: 18)
        
        addSubview(label)
        
        label.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            label.widthAnchor.constraint(equalTo: self.widthAnchor, constant: -(Constants.Label.margin*2)),
            label.heightAnchor.constraint(equalTo: self.heightAnchor, constant: -Constants.Label.top),
            label.topAnchor.constraint(equalTo: self.topAnchor, constant: Constants.Label.top),
            label.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: Constants.Label.margin),
        ])
    }
    
    func configure(text: String) {
        label.text = text
    }
}

private enum Constants {
    
    enum Label {
        static let margin: CGFloat = 24
        static let top: CGFloat = 14
    }
}

