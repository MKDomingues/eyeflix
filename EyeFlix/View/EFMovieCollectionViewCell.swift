
import UIKit
import Kingfisher

final class EFMovieCollectionViewCell: UICollectionViewCell {
    
    private(set) var viewModel: EFMovieCollectionViewCellViewModel?
    private var imageView: UIImageView = UIImageView()
    
    func configure(viewModel: EFMovieCollectionViewCellViewModel) {
        
        self.viewModel = viewModel
        self.backgroundColor = .EFDarkBackground
    
        setImageView()
        setImage()
    }
    
    
    private func setImageView() {
        
        addSubview(imageView)
        
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerRadius = Constants.Poster.cornerRadius
        imageView.clipsToBounds = true

        imageView.translatesAutoresizingMaskIntoConstraints = false
                
        NSLayoutConstraint.activate([
            imageView.widthAnchor.constraint(equalTo: self.widthAnchor),
            imageView.heightAnchor.constraint(equalTo: self.heightAnchor),
            imageView.topAnchor.constraint(equalTo: self.topAnchor),
            imageView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
        ])
    }
    
    private func setImage() {
        
        guard let posterURL = viewModel?.posterURL else {
            
            imageView.image = UIImage(named: "placeholder")
            return
        }
        
        imageView.kf.setImage(with: posterURL)
    }
    
    override func prepareForReuse() {
        
        imageView.image = nil
    }
}

private enum Constants {
    
    enum Poster {
     
        static let cornerRadius: CGFloat = 10
        static let margin: CGFloat = 4
    }
}
