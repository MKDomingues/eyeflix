
import UIKit
import Kingfisher

final class EFMovieViewController: UIViewController {

    private var viewModel: EFMoviewViewModel
    private var imageView: UIImageView = UIImageView()
    
    init(_ viewModel: EFMoviewViewModel) {
        
        self.viewModel = viewModel
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("🧨 init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .EFDarkBackground
        title = viewModel.navigationBarTitlePrefix + String(viewModel.movie.voteAverage)
        
        setImageView()
        setImage()
    }
    
    private func setImageView() {
        
        view.addSubview(imageView)
        imageView.contentMode = .scaleAspectFit
        imageView.translatesAutoresizingMaskIntoConstraints = false
                
        NSLayoutConstraint.activate([
            imageView.widthAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.widthAnchor, constant: -(Constants.Poster.margin*2)),
            imageView.heightAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.heightAnchor, constant: -(Constants.Poster.margin*2)),
            imageView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant:Constants.Poster.margin),
            imageView.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor, constant:Constants.Poster.margin),
        ])
    }
    
    private func setImage() {
        
        guard let posterURL = viewModel.posterURL else {
            
            imageView.image = UIImage(named: "placeholder")
            return
        }
        
        imageView.kf.setImage(
            with: posterURL,
            placeholder: UIImage(named: "placeholder"),
            options: [.transition(.fade(0.2))]
        )
    }
}

private enum Constants {
    
    enum Poster {
     
        static let margin: CGFloat = 24
    }
}
