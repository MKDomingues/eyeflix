
import UIKit

final class EFBrowseCollectionViewController: UICollectionViewController {

    weak var coordinator: EFMainCoordinatorRepresentable?
    private var viewModel: EFBrowseViewModel
    private var spinner = UIActivityIndicatorView(style: .large)
    
    init(_ viewModel: EFBrowseViewModel) {
        
        self.viewModel = viewModel
        super.init(collectionViewLayout: UICollectionViewFlowLayout())
    }
    
    required init?(coder: NSCoder) {
        fatalError("🧨 init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = viewModel.navigationBarTitle
        
        collectionView.collectionViewLayout = getCollectionViewLayout()
        collectionView.backgroundColor = .EFDarkBackground
        collectionView.isHidden = true
        
        setSpinner()
        viewModel.delegate = self
        registerCells()
        viewModel.loadData()
    }

    func registerCells() {
        
        collectionView.register(EFMovieCollectionViewCell.self, forCellWithReuseIdentifier: Constants.Cell.identifier)
        
        collectionView.register(
            EFCollectionViewHeader.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: Constants.Header.identifier
        )
    }
    
    func setSpinner() {
        
        view.addSubview(spinner)
        
        spinner.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            spinner.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            spinner.centerYAnchor.constraint(equalTo: view.centerYAnchor)
        ])
        
        spinner.startAnimating()
    }
    
    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        viewModel.sectionData.count
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        viewModel.sectionData[section].count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.Cell.identifier, for: indexPath) as? EFMovieCollectionViewCell else { fatalError("🧨 Unable to dequeue cell") }
        
        let movie = viewModel.sectionData[indexPath.section][indexPath.row]
        let viewModel = EFViewModelFactory.makeMovieCellViewModel(with: movie)
        
        cell.configure(viewModel: viewModel)
    
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        guard
            let cell = collectionView.cellForItem(at: indexPath) as? EFMovieCollectionViewCell,
            let movie = cell.viewModel?.movie
        else {
            fatalError("🧨 Unexpected cell class or nil viewModel")
        }

        viewModel.isValidMovie(movie)
            ? pushMovieViewController(with: movie)
            : displayMovieScoreAlert(with: movie.voteAverage)
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let collectionViewHeader = collectionView.dequeueReusableSupplementaryView(
            ofKind: UICollectionView.elementKindSectionHeader,
            withReuseIdentifier: Constants.Header.identifier,
            for: indexPath
        ) as? EFCollectionViewHeader

        guard let header = collectionViewHeader else {

            fatalError("🧨 Unable to dequeue header")
        }
        
        header.frame = CGRect(x: 0, y: 0, width: view.bounds.width, height: Constants.Header.height)
        header.configure(text: viewModel.sectionTitles[indexPath.section])

        return header
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {

        collectionView.collectionViewLayout = getCollectionViewLayout()
        collectionView.collectionViewLayout.invalidateLayout()
    }
    
    private func getCollectionViewLayout() -> UICollectionViewCompositionalLayout {
        
        let orientation: UIDeviceOrientation = UIDevice.current.orientation
        
        let itemSize = NSCollectionLayoutSize(
            widthDimension: .fractionalWidth(orientation.isPortrait ? Constants.Layout.Portrait.itemWidth : Constants.Layout.Landscape.itemWidth),
            heightDimension: .fractionalHeight(orientation.isPortrait ? Constants.Layout.Portrait.itemHeight : Constants.Layout.Landscape.itemHeight)
        )
                
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        item.contentInsets = Constants.Cell.insets
        
        let groupSize = NSCollectionLayoutSize(
            widthDimension: .fractionalWidth(orientation.isPortrait ? Constants.Layout.Portrait.groupWidth : Constants.Layout.Landscape.groupWidth),
            heightDimension: .fractionalHeight(orientation.isPortrait ? Constants.Layout.Portrait.groupHeight : Constants.Layout.Landscape.groupHeight)
        )
        
        let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize, subitem: item, count: Constants.Layout.groupCount)
        group.interItemSpacing = .none
    
        let section = NSCollectionLayoutSection(group: group)
        section.orthogonalScrollingBehavior = .continuous
        section.interGroupSpacing = Constants.Layout.interGroupSpacing
        
        let headerSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(Constants.Header.width), heightDimension: .absolute(Constants.Header.height))

        section.boundarySupplementaryItems = [
            NSCollectionLayoutBoundarySupplementaryItem(layoutSize: headerSize, elementKind: Constants.Header.identifier, alignment: .top)
        ]
                
        return UICollectionViewCompositionalLayout(section: section)
    }
    
    // MARK: User Interaction
    
    func displayMovieScoreAlert(with score: Double) {
        
        EFAlert.displayScoreAlert(inViewController: self, score: score)
    }
    
    func pushMovieViewController(with movie: EFMovie) {
        
        guard let coordinator = coordinator else { return }
        
        coordinator.pushMovieViewController(with: movie)
    }
}

extension EFBrowseCollectionViewController: EFBrowseViewModelDelegate {
    
    func displayErrorAlert(_ type: EFError) {
        
        DispatchQueue.main.async {
            
            EFAlert.displayAlert(inViewController: self, errorType: type, handler: { [weak self] _ in
                
                guard let self = self else { return }
                self.viewModel.retryRequest()
            })
        }
    }
    
    func reloadCollectionViewData() {
        
        DispatchQueue.main.async {
            
            if self.spinner.isAnimating {
                self.spinner.stopAnimating()
            }
            
            self.collectionView.reloadData()
            
            self.collectionView.alpha = 0.0
            self.collectionView.isHidden = false

            self.view.layoutIfNeeded()
            UIView.animate(withDuration: Constants.Animation.duration, delay: Constants.Animation.delay) {

                self.collectionView.alpha = 1.0
                self.view.layoutIfNeeded()
            }
        }
    }
}

private enum Constants {
    
    enum Header {
        static let identifier = "EFCollectionViewHeaderID"
        static let height: CGFloat = 44
        static let width: CGFloat = 1.0
    }
    
    enum Cell {
        static let identifier = "EFMovieCellID"
        static let insets = NSDirectionalEdgeInsets(top: 8, leading: 8, bottom: 8, trailing: 8)
    }
    
    enum Animation {
        static let duration: TimeInterval = 0.5
        static let delay: TimeInterval = 0.4
    }
    
    enum Layout {
        static let groupCount = 3
        static let interGroupSpacing: CGFloat = 0
        
        enum Landscape {
            static let itemHeight: CGFloat = 1.0
            static let itemWidth: CGFloat = 0.7
            static let groupHeight: CGFloat = 0.5
            static let groupWidth: CGFloat = 0.5
        }
        
        enum Portrait {
            static let itemHeight: CGFloat = 1.0
            static let itemWidth: CGFloat = 0.23
            static let groupHeight: CGFloat = 0.23
            static let groupWidth: CGFloat = 1.0
        }
    }
}
