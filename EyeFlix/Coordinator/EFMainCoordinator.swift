import UIKit

protocol EFMainCoordinatorRepresentable: class {
    
    func start(_ navigationController: UINavigationController)
    func pushMovieViewController(with movie: EFMovie)
}

class EFMainCoordinator: EFMainCoordinatorRepresentable, EFCoordinatorRepresentable {
    
    private(set) var childCoordinators = [EFCoordinatorRepresentable]()
    weak var navigationController: UINavigationController?
    
    func start(_ navigationController: UINavigationController) {
        
        self.navigationController = navigationController
        let viewController = EFViewControllerFactory.makeBrowseViewController(with: self)
        
        navigationController.pushViewController(viewController, animated: false)
    }
    
    func pushMovieViewController(with movie: EFMovie) {
        
        let viewController = EFViewControllerFactory.makeMovieViewController(with: movie)
        
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func addChildCoordinator(_ coordinator: EFCoordinatorRepresentable) {
        
        childCoordinators.append(coordinator)
    }
    
    func removeAllChildCoordinators() {
     
        childCoordinators.removeAll()
    }
}
