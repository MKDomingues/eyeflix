import UIKit

protocol EFCoordinatorRepresentable: class {

    func addChildCoordinator(_ coordinator: EFCoordinatorRepresentable)
    func removeAllChildCoordinators()
}
