
import Foundation

enum EFPosterSize {
    
    case thumbnail
    case large
}

struct EFMovieCollectionViewCellViewModel {
    
    private(set) var movie: EFMovie
    private(set) var posterURL: URL?
    
    init(with movie: EFMovie) {
             
        self.movie = movie
        self.posterURL = EFRequestURL.getURL(for: .poster(URLComponent: movie.posterPath, size: .thumbnail))
    }
}
