
import Foundation

struct EFMoviewViewModel {
    
    private(set) var movie: EFMovie
    private(set) var posterURL: URL?
    private(set) var navigationBarTitlePrefix = Constants.navigationBarTitlePrefix
    
    init(with movie: EFMovie) {
        
        self.movie = movie
        self.posterURL = EFRequestURL.getURL(for: .poster(URLComponent: movie.posterPath, size: .large))
    }
}

private enum Constants {
    
    static let navigationBarTitlePrefix = "EyeFlix score: "
}
