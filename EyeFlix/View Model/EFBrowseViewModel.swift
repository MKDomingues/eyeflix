
import Foundation

protocol EFBrowseViewModelDelegate: class {
    
    func displayErrorAlert(_ type: EFError)
    func reloadCollectionViewData()
}

final class EFBrowseViewModel {
    
    weak var delegate: EFBrowseViewModelDelegate?
    private var service: EFMovieServiceRepresentable
    
    private(set) var sectionData: [[EFMovie]] = []
    private(set) var sectionTitles = Constants.sectionTitles
    private(set) var navigationBarTitle = Constants.navigationBarTitle
    private(set) var performRequest: Bool = true
    
    private var movieResults: [EFMovies] {
        
        didSet {
            guard movieResults.count == Constants.sectionCount else { return }
            
            sectionData = movieResults
                .sorted(by: { $0.page < $1.page })
                .map { $0.results }
            
            if let delegate = delegate { delegate.reloadCollectionViewData() }
        }
    }
    
    private(set) var serviceError: EFError {
        
        didSet {
            self.performRequest =  false
            if let delegate = self.delegate { delegate.displayErrorAlert(serviceError) }
        }
    }
    
    init(with service: EFMovieServiceRepresentable) {
        
        self.service = service
        self.movieResults = []
        self.serviceError = .none
    }
    
    func loadData() {
        
        var counter = 1
        while performRequest, counter <= Constants.sectionCount {
            
            getMovies(for: counter)
            counter += 1
        }
    }
    
    func retryRequest() {
        
        performRequest = true
        movieResults.removeAll()
        
        loadData()
    }
    
    func isValidMovie(_ movie: EFMovie) -> Bool {
        
        movie.voteAverage >= Constants.validScore
    }
    
    // MARK: Private
    
    private func getMovies(for page:Int) {

        service.getMovies(for: page) { [weak self] (result) in
            
            guard let self = self else { return }
            
            if case .failure(let error) = result {
             
                self.serviceError = error
                return
            }
            
            if case .success(let movies) = result {
       
                self.movieResults.append(movies)
            }
        }
    }
}

private enum Constants {
    
    static let navigationBarTitle = "Browse Movies"
    
    static let sectionCount = 8
    
    static let validScore: Double = 6
    
    static let sectionTitles = [
        "Popular on EyeFlix",
        "Top 10 in Portugal",
        "Trending Now",
        "New and fresh",
        "What your neighbours are watching",
        "Recommended for you",
        "More movies to look at for half an hour",
        "Stuff no one watches"
    ]
}
