
import UIKit

enum EFAlert {
    
    static func displayAlert(inViewController viewController: UIViewController, errorType: EFError, handler:@escaping ((UIAlertAction)->Void)) {
        
        var errorMessage = ""
        
        switch errorType {
        case .typeData:
            errorMessage = Constants.message.typeData
        case .typeRequest(let description):
            errorMessage = description
        case .typeParsing:
            errorMessage = Constants.message.typeParsing
        case .typeURL:
            errorMessage = Constants.message.typeURL
        case .none: break
        }
        
        let alert = UIAlertController(title: "Error", message: errorMessage, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Retry", style: .default, handler: handler))
    
        viewController.present(alert, animated: true, completion: nil)
    }
    
    static func displayScoreAlert(inViewController viewController: UIViewController, score: Double) {
        
        let alert = UIAlertController(title: "Insuficient Score (\(score))", message: Constants.message.typeScore, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        
        viewController.present(alert, animated: true, completion: nil)
    }
}

private enum Constants {

    enum message {
        
        static let typeData = "Failed to get movies.\nPlease try again."
        static let typeParsing = "Failed to configure movie information.\nPlease try again."
        static let typeURL =  "Failed to get request URL.\nPlease try again."
        
        static let typeScore = "Looks like this movie is not worth watching.\nPlease choose another."
    }
}


