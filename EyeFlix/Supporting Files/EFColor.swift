import UIKit

extension UIColor {
    
    convenience init(red: Int, green: Int, blue: Int, a: CGFloat = 1.0) {
        self.init(
            red: CGFloat(red) / 255.0,
            green: CGFloat(green) / 255.0,
            blue: CGFloat(blue) / 255.0,
            alpha: a
        )
    }
    
    static let EFDarkBackground = UIColor(red: 21, green: 21, blue: 21)
    static let EFNavigationRed = UIColor(red: 213, green: 0, blue: 0)
    static let EFTextFadedRed = UIColor(red: 251, green: 233, blue: 233)
}
