enum EFError: Error, Equatable {
    
    case typeData
    case typeRequest(description: String)
    case typeParsing
    case typeURL
    case none
}
