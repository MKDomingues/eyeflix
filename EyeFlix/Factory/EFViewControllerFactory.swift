
import Foundation

protocol EFViewControllerFactoryRepresentable {
    
    static func makeBrowseViewController(with coordinator: EFMainCoordinatorRepresentable) -> EFBrowseCollectionViewController
    static func makeMovieViewController(with movie: EFMovie) -> EFMovieViewController
}

class EFViewControllerFactory: EFViewControllerFactoryRepresentable {
    
    static func makeBrowseViewController(with coordinator: EFMainCoordinatorRepresentable) -> EFBrowseCollectionViewController {
        
        let viewModel = EFViewModelFactory.makeBrowseViewModel()
        let viewController = EFBrowseCollectionViewController(viewModel)
        viewController.coordinator = coordinator
        
        return viewController
    }
    
    static func makeMovieViewController(with movie: EFMovie) -> EFMovieViewController {
        
        let viewModel = EFViewModelFactory.makeMovieViewModel(with: movie)
        let viewController = EFMovieViewController(viewModel)
        
        return viewController
    }
}
