
import Foundation

protocol EFViewModelFactoryRepresentable {
    
    static func makeBrowseViewModel() -> EFBrowseViewModel
    static func makeMovieViewModel(with movie: EFMovie) -> EFMoviewViewModel
}

class EFViewModelFactory: EFViewModelFactoryRepresentable {
 
    static func makeBrowseViewModel() -> EFBrowseViewModel {
        
        EFBrowseViewModel(with: EFMovieService())
    }
    
    static func makeMovieViewModel(with movie: EFMovie) -> EFMoviewViewModel {
        
        EFMoviewViewModel(with: movie)
    }
    
    static func makeMovieCellViewModel(with movie: EFMovie) -> EFMovieCollectionViewCellViewModel {
        
        EFMovieCollectionViewCellViewModel(with: movie)
    }
}
