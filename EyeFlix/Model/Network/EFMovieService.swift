import Foundation

protocol EFServiceRepresentable {

    func getMovies(for page: Int, completion: @escaping MovieClosure)
}

protocol EFMovieServiceRepresentable {

    func getMovies(for page: Int, completion: @escaping MovieClosure)
}

typealias MovieClosure = (Result<EFMovies, EFError>) -> Void

class EFMovieService: EFMovieServiceRepresentable {
    
    private let session = URLSession(configuration: .default)
    private var dataTask: URLSessionDataTask?
    
    func getMovies(for page: Int, completion: @escaping MovieClosure) {
        
        guard let requestURL = EFRequestURL.getURL(for: .movie(page: page)) else {
            completion(.failure(.typeURL))
            return
        }
        
        dataTask = session.dataTask(with: requestURL) { data, response, error in
            
            if let error = error {
                completion(.failure(.typeRequest(description: error.localizedDescription)))
                return
            }
            
            guard let data = data else {
                
                completion(.failure(.typeData))
                return
            }
            
            do {
                let decoder = JSONDecoder()
                decoder.keyDecodingStrategy = .convertFromSnakeCase
                
                let browseResults: EFMovies = try decoder.decode(EFMovies.self, from: data)
                
                completion(.success(browseResults))
                
            } catch {
                completion(.failure(.typeParsing))
            }
        }
        dataTask?.resume()
    }
}
