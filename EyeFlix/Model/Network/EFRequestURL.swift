
import UIKit

enum RequestType {
    
    case movie(page: Int)
    case poster(URLComponent: String, size: EFPosterSize)
}

enum EFRequestURL {
    
    static func getURL(for requestType: RequestType) -> URL? {
        
        var components = URLComponents()
        
        components.scheme = "https"
        components.host = getHost(requestType)
        components.path = getPath(requestType)
        
        if let queryItems = getQueryItems(requestType) {
            components.queryItems = queryItems
        }
        
        guard let requestURL = components.url else { return nil }
        
        return requestURL
    }
    
    // MARK: Private
    
    private static func getHost(_ type: RequestType) -> String {
        
        switch type {
        case .movie:
            return Constants.Movie.host
        case .poster:
            return Constants.Poster.host
        }
    }
    
    private static func getPath(_ type: RequestType) -> String {
        
        switch type {
        case .movie:
            return Constants.Movie.path
            
        case let .poster(URLComponent, size):
            
            return Constants.Poster.path
                + (size == .thumbnail ? Constants.Poster.thumbnail : Constants.Poster.large)
                + URLComponent
        }
    }
    
    private static func getQueryItems(_ type: RequestType) -> [URLQueryItem]? {
        
        switch type {
        case .movie(let page):
            
            return [
                URLQueryItem(name: "page", value: String(page)),
                URLQueryItem(name: "api_key", value: Constants.API.key)
            ]
            
        case .poster:
            
            return nil
        }
    }
}

private enum Constants {
    
    enum Movie {
        static let host = "api.themoviedb.org"
        static let path = "/3/discover/movie"
    }
    
    enum Poster {
        static let host = "image.tmdb.org"
        static let path = "/t/p"
        static let thumbnail = "/w185"
        static let large = "/w780"
    }
    
    enum API {
        static let obfuscatedApiKey: [UInt8] =
            [214, 156, 124, 193, 95, 128, 186, 172, 20, 44, 193, 48, 142, 88, 165, 179, 163, 55, 122, 248, 37, 79, 124, 3, 93, 229, 137, 220, 170, 47, 34, 117, 226, 248, 74, 162, 105, 178, 217, 154, 45, 27, 241, 85, 236, 106, 195, 133, 150, 14, 76, 192, 67, 46, 29, 54, 60, 214, 184, 190, 159, 77, 20, 22]
        
        static var key: String {
            return String(bytes: obfuscatedApiKey.deobfuscated, encoding: .utf8)!
        }
    }
}

extension Array where Element == UInt8 {
    var deobfuscated: [UInt8] {
        let a = prefix(count / 2)
        let b = suffix(count / 2)
        return zip(a, b).map(^)
    }
}
