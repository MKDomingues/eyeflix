
struct EFMovies: Codable {
    
    let page: Int
    let results: [EFMovie]
    let totalPages: Int
    let totalResults: Int
}

struct EFMovie: Codable {
    
    let posterPath: String
    let voteAverage: Double
}


