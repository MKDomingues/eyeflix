
import Foundation

@testable import EyeFlix

final class EFMockMovieService: EFMovieServiceRepresentable {
    
    var movieResult: Result<EFMovies, EFError> = .success(EFMovies.mocked())
    
    func getMovies(for page: Int, completion: @escaping MovieClosure) {
    
        completion(movieResult)
    }
}

private extension EFMovies {
    
    static func mocked() -> Self {
        
        return self.init(
            page: 0,
            results: [EFMovie(posterPath: "", voteAverage: 0.0)],
            totalPages: 0,
            totalResults: 0)
    }
}
