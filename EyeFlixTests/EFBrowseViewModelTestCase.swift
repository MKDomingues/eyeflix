
import XCTest

@testable import EyeFlix

class EFBrowseViewModelTestCase: XCTestCase {

    var viewModel: EFBrowseViewModel!
    var mockService: EFMockMovieService!
    
    override func setUp() {
        
        mockService = EFMockMovieService()
        viewModel = .init(with: mockService)
    }
    
    override func tearDown() {
         
        viewModel = nil
        mockService = nil
    }
    
    func test_getMovies_withSuccess_shouldContinueRequest() {
        
        XCTAssertTrue(viewModel.performRequest)
        
        viewModel.loadData()
        
        XCTAssertTrue(viewModel.performRequest)
    }
    
    func test_getMovies_withFailure_shouldInterruptRequest() {
        
        mockService.movieResult = .failure(.typeURL)
        viewModel.loadData()
        
        XCTAssertFalse(viewModel.performRequest)
    }
    
    func test_getMovies_withSpecificFailure_shouldSetCorrectError() {
        
        XCTAssertEqual(viewModel.serviceError, .none)
        
        mockService.movieResult = .failure(.typeParsing)
        viewModel.loadData()
        
        XCTAssertEqual(viewModel.serviceError, .typeParsing)
    }
    
    func test_getMovies_withSuccess_shouldCollectExpectedResultCount() {
        
        XCTAssertEqual(viewModel.sectionData.count, 0)
        
        viewModel.loadData()
        
        XCTAssertEqual(viewModel.sectionData.count, 8)
    }
    
    func test_isValidMovie_withScore_shouldReturnExpectedValue() {
        
        XCTAssertTrue(
            viewModel.isValidMovie(
                EFMovie(posterPath: "", voteAverage: 6.0)
            )
        )
        
        XCTAssertTrue(
            viewModel.isValidMovie(
                EFMovie(posterPath: "", voteAverage: 8.8)
            )
        )
        
        XCTAssertFalse(
            viewModel.isValidMovie(
                EFMovie(posterPath: "", voteAverage: 3.7)
            )
        )
    }
}
